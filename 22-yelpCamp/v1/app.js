// yelpcamp/v1/app.js

// init packages and set defaults //

// import express
var express = require("express");
    // import body-parser
    bodyParser = require('body-parser');
// init express
var app = express();
app.use(bodyParser.urlencoded({extended: true}));
// set ejs as render engine
app.set('view engine', 'ejs');


// Stub data until db implimentation
var campgrounds = [
    { name: "Salmon Creek", image: "https://www.nps.gov/havo/planyourvisit/images/Namakanipaio_960.jpg?maxwidth=1200&maxheight=1200&autorotate=false" },
    { name: "East Fork", image: "https://www.nps.gov/glba/planyourvisit/images/campgoundentrance.JPG?maxwidth=1200&maxheight=1200&autorotate=false" },
    { name: "Yellow Stone", image: "https://www.nps.gov/lavo/planyourvisit/images/Lost-Creek-Campground.jpg?maxwidth=1200&maxheight=1200&autorotate=false" },
    { name: "Pacific Cost Trail", image: "https://www.nps.gov/lavo/planyourvisit/images/Summit-Lake-Campground.jpg?maxwidth=1200&maxheight=1200&autorotate=false" },
    { name: "Salmon Creek", image: "https://www.nps.gov/havo/planyourvisit/images/Namakanipaio_960.jpg?maxwidth=1200&maxheight=1200&autorotate=false" },
    { name: "East Fork", image: "https://www.nps.gov/glba/planyourvisit/images/campgoundentrance.JPG?maxwidth=1200&maxheight=1200&autorotate=false" },
    { name: "Yellow Stone", image: "https://www.nps.gov/lavo/planyourvisit/images/Lost-Creek-Campground.jpg?maxwidth=1200&maxheight=1200&autorotate=false" },
    { name: "Pacific Cost Trail", image: "https://www.nps.gov/lavo/planyourvisit/images/Summit-Lake-Campground.jpg?maxwidth=1200&maxheight=1200&autorotate=false" },
    { name: "Salmon Creek", image: "https://www.nps.gov/havo/planyourvisit/images/Namakanipaio_960.jpg?maxwidth=1200&maxheight=1200&autorotate=false" },
    { name: "East Fork", image: "https://www.nps.gov/glba/planyourvisit/images/campgoundentrance.JPG?maxwidth=1200&maxheight=1200&autorotate=false" },
    { name: "Yellow Stone", image: "https://www.nps.gov/lavo/planyourvisit/images/Lost-Creek-Campground.jpg?maxwidth=1200&maxheight=1200&autorotate=false" },
    { name: "Pacific Cost Trail", image: "https://www.nps.gov/lavo/planyourvisit/images/Summit-Lake-Campground.jpg?maxwidth=1200&maxheight=1200&autorotate=false" },
    { name: "Salmon Creek", image: "https://www.nps.gov/havo/planyourvisit/images/Namakanipaio_960.jpg?maxwidth=1200&maxheight=1200&autorotate=false" },
    { name: "East Fork", image: "https://www.nps.gov/glba/planyourvisit/images/campgoundentrance.JPG?maxwidth=1200&maxheight=1200&autorotate=false" },
    { name: "Yellow Stone", image: "https://www.nps.gov/lavo/planyourvisit/images/Lost-Creek-Campground.jpg?maxwidth=1200&maxheight=1200&autorotate=false" },
    { name: "Pacific Cost Trail", image: "https://www.nps.gov/lavo/planyourvisit/images/Summit-Lake-Campground.jpg?maxwidth=1200&maxheight=1200&autorotate=false" },
];


// Routes //

// Landing Page
app.get('/', function (req, res) {
    res.render("landing");
});

// Camp grounds
app.get('/campgrounds', function (req, res) {
    // render the campgrounds template with campgrounds data
    res.render('campgrounds', {campgrounds: campgrounds});
});

// add new campground
app.get('/campgrounds/new', function(req,res) {
    // show new campground form.
    res.render('newcampground')
})

// Add a campground
app.post('/campgrounds/', function(req, res) {
    // get data from form and add to campgrounds array
    var newName = req.body.name;
    var newImage = req.body.image;
    var newCampground = {name: newName, image: newImage};
    campgrounds.push(newCampground);
    res.redirect('/campgrounds');
    // redirect back to campgrounds page
});

// start the server and listen
app.listen('8000', function () {
    console.log("Server started.");
});
