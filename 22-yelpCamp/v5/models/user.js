// USER //

var mongoose = require("mongoose");

// define user model
var userSchema = new mongoose.Schema({
    // setting email type to string and it has to be unique throughout the whole DB
    email: {type: String, unique: true},
    name: String,
    // the posts 
    posts: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Post"
    }]
})

// init model and export
module.exports = mongoose.model("User", userSchema)

/*
checking if email is unique before saving
```js
user.save(function(err) {
    if(err) {
        console.log(err)
        // mongo error code for duplicate entry of unique field in DB
        if(err.code === 11000) {
        err = "That email is already in use. Please use another one or sign in."
        }
    }
    res.render("register", {error: error})
    else {
        res.redirect("/dashboard")
    }
})
```
 */