// app.js 

// first Node web app

// import packages
var express = require("express");

// execute and save express to var named app
var app = express();

// routes

// "/" => "Hi there!"
app.get("/", function(req, res) {
    res.send("Hi there!");
    console.log("/ - route used.");
}) 

// "/bye" => "See ya!"
app.get("/bye", function(req, res) {
    res.send("See ya!");
}) 

// "/dog" == "WOOF!"
app.get("/dogs", function(req, res) {
    res.send("WOOF!");
}) 

app.get("/r/:subredditName", function(req, res) {
    var subreddit = req.params.subredditName;
    res.send("Welcome to the " + subreddit.toUpperCase() + " subreddit");
})

// Page not found
// Must at the end due to Route Order
app.get("*", function(req, res) {
    res.send("404");
})

app.listen(8000, function() {
    console.log("app.js is running");
})