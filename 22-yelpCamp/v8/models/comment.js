// models/comment.js

// define the comment model // 

// require mongoose
var mongoose = require("mongoose");

// set the comment schema
var commentSchema = new mongoose.Schema({
    text: String,
    // embed author information from the Users collection
    author: {
        // look up author id from the Users collection
        id: { 
            type: mongoose.Schema.Types.ObjectId, 
            ref: "User" 
        },
        // embed username. It's more efficent this way since the frontend is looping over comments
        // +printing out the text, might as well give it the author's username rather than querying
        // +the DB everytime. This is only possible with a non-relational DB. Embeded for the win.
        username: String
    }
});

// init and export the model
module.exports = mongoose.model("Comment", commentSchema);


