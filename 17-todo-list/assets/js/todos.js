// CRUD for TODO App

// Create //

// toggle the form with the plus sign
$(".fa-plus").click(function() {
	$("input[type='text']").fadeToggle();
})

// listen to the input
$("input[type='text']").keypress(function(event) {
    // listen for the "Enter" key
    if(event.which === 13) {
        // grabbing new todo text from input
        var NewTodoText = ($(this).val());
        // clear the input
        $(this).val("");
        // create new li from the NewTodoText input and add to the ul
        $("ul").append("<li><span><i class='fas fa-trash-alt'></i></span> " + NewTodoText + "</li>");
    };
});

// Read //

// Nothing in Read as this is browser-based

// Update //

// Check off specific todo's by clicking
// on() still only works on elements that exist at page load. Need
//+ to listen to the parent and define the child within the on() object,
//+ i.e.: ```$("ul").on("click", "li", function() {```, rather than ```$("li").on("click", function() {```
$("ul").on("click", "li", function() {
    // class defined in CSS
    $(this).toggleClass("completed");
});


// Delete // 

// Click on the trash can to delete todo
$("ul").on("click", "span", function(event) {
    // parent being removed is the li, being that the affected span is within an li
    $(this).parent().fadeOut(1000, function() {
        $(this).remove();
    });
    // stops event propagation
    event.stopPropagation();
});

