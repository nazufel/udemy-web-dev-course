var mongoose = require("mongoose");

var Post = require("./models/post");
var User = require("./models/user");

// DB AND MODEL //

// connect to the db
mongoose.connect('mongodb://root:root@localhost:8081/datareference?authSource=admin', { useNewUrlParser: true });

/*
// find user and find all posts for such user
// find a user by their email, reach out to the posts collection and get all their posts, and then execute the query
User.findOne({email: "hermione@hogwarts.edu"}).populate("posts").exec(function(err,user){
    if(err) {
        console.log(err)
    } else {
        console.log(user)
    }
})
*/

/*
Post.create({
    title: "Being President of SPEW",
    content: "The book starts out with..."
}, function(err, post) {
    User.findOne({email: "hermione@hogwarts.edu"}, function(err, foundUser) {
        if(err) {
            console.log(err)
        } else {
            foundUser.posts.push(post);
            foundUser.save(function(err, data){
                if(err) {
                    console.log(err) 
                } else {
                    console.log(data)
                }
            })
        }
    })
})

/*
// defining new user
User.create({
    email: "hermione@hogwarts.edu",
    name: "Hermione Granger"
})


// defining new post and pushing into posts array
newUser.posts.push({
    title: "How to brew polyjuice potion",
    content: "Just kidding. Go to potions class."
})

// saving to the db
newUser.save(function(err, user) {
    if(err)  {
        console.log(err)
    } else {
        console.log(user)
    }
})

*/

/*
var newPost = new Post({
    title: "Reflections on Apples",
    content: "They are delicious"
})

newPost.save(function(err, post) {
    if(err) {
        console.log(err)
    } else {
        console.log(post)
    }
})
*/

/*
User.findOne({name: "Hermione Granger"}, function(err, user) {
    if(err) {
        console.log(err)
    } else {
        user.posts.push({
            title: "Three things i really hate",
            content: "Voldemort. Voldemort. Voldemort."
        })
        user.save(function(err, user) {
            if(err) {
                console.log(err) 
            } else {
                console.log(user)
            }
        })
    }
})
*/