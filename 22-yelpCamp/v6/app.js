// yelpcamp/v1/app.js

// import packages
var express = require("express"),
    app = express(),
    bodyParser = require('body-parser'),
    Campground = require("./models/campground"),
    Comment = require("./models/comment"),
    LocalStrategy = require("passport-local"),
    passport = require('passport'),
    seedDB = require("./seeds"),
    User = require("./models/user"),


// import mongoose
mongoose = require('mongoose');

// init packages and set defaults //

// init express


// init body-parser
app.use(bodyParser.urlencoded({ extended: true }));

// passport config //

app.use(require("express-session")({
    secret: "Expecto Potronum",
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());
// middleware to pass logged in user to every route
app.use(function(req,res,next) {
    // whatever is in res.locals is available to the templates
    res.locals.currentUser = req.user;
    // do the next thing in the calling function/handler
    next();
});
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

// set ejs as render engine
app.set('view engine', 'ejs');

// use the custom files in the public directory
app.use(express.static(__dirname + "/public/"))
// Database stuff //

// connect to the db
mongoose.connect('mongodb://root:root@localhost:8081/yelpcamp?authSource=admin', { useNewUrlParser: true });

// seed the DB
seedDB();

// Routes //

// Landing Page
app.get('/', function (req, res) {
    res.render("landing");
});

// INDEX - Get all Campgrounds
app.get('/campgrounds', function (req, res) {
    // get logged in user info
    // find methods require the collection to search through. 
    Campground.find({}, function (err, campgrounds) {
        if (err) {
            console.log("These aren't the droids you're looking for.");
        } else {
            // render the campgrounds template with campgrounds and user data via req.locals.currentUser middleware
            res.render('campgrounds/index', { campgrounds: campgrounds });
        }
    });
});

// NEW - Display creation form
// using isLoggedIn middleware to make sure users are logged to create new campgrounds
app.get('/campgrounds/new', isLoggedIn, function (req, res) {
    // show new campground form.
    res.render('campgrounds/newcampground')
})

// CREATE - Add a campground
// using isLoggedIn middleware to make sure users are logged to create new campgrounds
app.post("/campgrounds", isLoggedIn, function (req, res) {
    console.log("hit the post route")
    // get data from the form and add to campgrounds array
    var name = req.body.name;
    var image = req.body.image;
    var desc = req.body.description;
    // make an object from request body fields to be created in the db 
    var newCampground = { name: name, image: image, description: desc };
    // create a new campground and save to db
    Campground.create(newCampground, function (err, newCampground) {
        // check the error
        if (err) {
            console.log("error");
            // log the object and redirect
        } else {
            console.log("Create successful:");
            // redirect back to campgrounds page
            res.redirect("/campgrounds");
        }
    });
});

// SHOW - shows one particular campground
// note, the url param /:id could be anything, thus must be last as
//+ /campgrounds/new could be processed as an ID by the router, rather than a route
app.get("/campgrounds/:id", function (req, res) {
    // grab the id, retrive the associated comments, and execute the rest of the call back.
    Campground.findById(req.params.id).populate("comments").exec(function (err, foundCampground) {
        if (err) {
            console.log(err);
        } else {
            console.log(foundCampground)
            // render the template and pass in the data
            res.render("campgrounds/show", { campground: foundCampground });
        }
    });
});

// =======  COMMENTS ======== //

// show comment form
// using isLoggedIn middleware to check if users are logged in
app.get("/campgrounds/:id/comments/new", isLoggedIn, function (req, res) {
    // find campground by id
    Campground.findById(req.params.id, function (err, campground) {
        if (err) {
            console.log(err)
        } else {
            res.render("comments/new", { campground: campground });
        }
    })
})

// submit the form
// using isLoggedIn middleware to make sure users are logged to create new comments
app.post("/campgrounds/:id/comments", isLoggedIn, function (req, res) {
    //look up campground by ID
    Campground.findById(req.params.id, function (err, campground) {
        if (err) {
            console.log(err)
            res.redirect("/campgrounds")
        } else {
            // create a new comment
            Comment.create(req.body.comment, function (err, comment) {
                if (err) {
                    console.log(err)
                } else {
                    // connect new comment to campground
                    campground.comments.push(comment)
                    campground.save()
                    // redirect to show page
                    res.redirect("/campgrounds/" + campground._id)
                }
            })
        }
    })
})

// auth routes //

// show register form
app.get("/register", function(req,res) {
    res.render("register");
});

// singup
app.post("/register", function(req,res) {
    // grab the username from the form and make a new user object
    var newUser = new User({username: req.body.username})
    // session information and save to the db
    User.register(newUser, req.body.password, function(err, user) {
        // hangle errors
        if(err) {
            console.log(err)
            return res.render("register")
        } 
        // use the local strategy to sign in and redirect to campgrounds
        passport.authenticate("local")(req,res,function() {
            res.redirect("/campgrounds")
        })
    }) 
})

// show login form
app.get("/login", function(req,res) {
    // render the form
    res.render("login")
})

// login logic
// Colt's code didn't work. copied this from http://www.passportjs.org/packages/passport-local/
app.post('/login', 
    // use passport's local strategy. failures redirect to /login
    passport.authenticate('local', { failureRedirect: '/login' }),
    function(req, res) {
        // success redirect back to campgrounds
        res.redirect('/campgrounds');
});

// logout logic
app.get("/logout", function(req,res) {
    // logout using the builtin method provided by passport
    req.logout();
    // redirect to campgrounds
    res.redirect("/campgrounds");
})

//-- MIDDLEWARE --//

// checking if users are logged in
function isLoggedIn(req, res, next) {
    // check if the user is authenticated
    if(req.isAuthenticated()) {
        // if so, return out and keep moving through the rest of the callback
        return next();
    }
    // redirect to login page
    res.redirect("/login");
}

// start the server and listen
app.listen('8000', function () {
    console.log("Server started.");
});
