// yelpcamp/v1/app.js

// init packages and set defaults //

// import express
var express = require("express");
// import body-parser
var bodyParser = require('body-parser');

var Campground = require("./models/campground");
var Comment = require("./models/comment");
var User = require("./models/user");
seedDB = require("./seeds")

// import mongoose
mongoose = require('mongoose');

// init express
var app = express();

// init body-parser
app.use(bodyParser.urlencoded({ extended: true }));

// set ejs as render engine
app.set('view engine', 'ejs');

// use the custom files in the public directory
app.use(express.static(__dirname + "/public/"))
// Database stuff //

// connect to the db
mongoose.connect('mongodb://root:root@localhost:8081/yelpcamp?authSource=admin', { useNewUrlParser: true });

// seed the DB
seedDB();

// Routes //

// Landing Page
app.get('/', function (req, res) {
    res.render("landing");
});

// INDEX - Get all Campgrounds
app.get('/campgrounds', function (req, res) {
    // find methods require the collection to search through. 
    Campground.find({}, function (err, campgrounds) {
        if (err) {
            console.log("These aren't the droids you're looking for.");
        } else {
            // render the campgrounds template with campgrounds data
            res.render('campgrounds/index', { campgrounds: campgrounds });
        }
    });
});

// NEW - Display creation form
app.get('/campgrounds/new', function (req, res) {
    // show new campground form.
    res.render('campgrounds/newcampground')
})

// CREATE - Add a campground
app.post("/campgrounds", function (req, res) {
    console.log("hit the post route")
    // get data from the form and add to campgrounds array
    var name = req.body.name;
    var image = req.body.image;
    var desc = req.body.description;
    // make an object from request body fields to be created in the db 
    var newCampground = { name: name, image: image, description: desc };
    // create a new campground and save to db
    Campground.create(newCampground, function (err, newCampground) {
        // check the error
        if (err) {
            console.log("error");
            // log the object and redirect
        } else {
            console.log("Create successful:");
            // redirect back to campgrounds page
            res.redirect("/campgrounds");
        }
    });
});

// SHOW - shows one particular campground
// note, the url param /:id could be anything, thus must be last as
//+ /campgrounds/new could be processed as an ID by the router, rather than a route
app.get("/campgrounds/:id", function (req, res) {
    // grab the id, retrive the associated comments, and execute the rest of the call back.
    Campground.findById(req.params.id).populate("comments").exec(function (err, foundCampground) {
        if (err) {
            console.log(err);
        } else {
            console.log(foundCampground)
            // render the template and pass in the data
            res.render("campgrounds/show", { campground: foundCampground });
        }
    });
});

// =======  COMMENTS ======== //

// show comment form
app.get("/campgrounds/:id/comments/new", function (req, res) {
    // find campground by id
    Campground.findById(req.params.id, function (err, campground) {
        if (err) {
            console.log(err)
        } else {
            res.render("comments/new", { campground: campground });
        }
    })
})

// submit the form
app.post("/campgrounds/:id/comments", function (req, res) {
    //look up campground by ID
    Campground.findById(req.params.id, function (err, campground) {
        if (err) {
            console.log(err)
            res.redirect("/campgrounds")
        } else {
            // create a new comment
            Comment.create(req.body.comment, function (err, comment) {
                if (err) {
                    console.log(err)
                } else {
                    // connect new comment to campground
                    campground.comments.push(comment)
                    campground.save()
                    // redirect to show page
                    res.redirect("/campgrounds/" + campground._id)
                }
            })
        }
    })
})

// start the server and listen
app.listen('8000', function () {
    console.log("Server started.");
});
