//25-auth/app.js

var express = require("express"),
    mongoose = require("mongoose"),
    passport = require("passport"),
    bodyParser = require("body-parser"),
    LocalStrategy = require("passport-local"),
    passportLocalMongoose = require("passport-local-mongoose"),
    User = require("./models/user");


// init express
var app = express();

// set ejs as render engine
app.set("view engine", "ejs");

app.use(bodyParser.urlencoded({ extended: true }));

// set session behavior
app.use(require("express-session")({
    // this secret should be hard-coded into production code
    secret: "I am become death, destroyer of worlds.",
    resave: false,
    saveUninitialized: false,
}));

// need both lines when using passport to init
app.use(passport.initialize());
app.use(passport.session());

// encode and decode the session
// these are some of those methods we get by adding the plugin in the user model file
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

// connect to the db
mongoose.connect('mongodb://root:root@localhost:8081/learningauth?authSource=admin', { useNewUrlParser: true });

// ROUTES //

// index
app.get("/", function (req, res) {
    res.render("home")
});

// show register form
app.get("/register", function (req, res) {
    res.render("register")
})

// register the user
app.post("/register", function (req, res) {
    req.body.username
    req.body.password
    // make a new user name object and save it to the db without a password. then, pass in the password and hash it.
    User.register(new User({ username: req.body.username }), req.body.password, function (err, user) {
        if (err) {
            console.log(err);
            // force the register template again since there's an error.
            return res.render("register");
        }
        // log the user in with a session using the local passport strategy.
        passport.authenticate("local")(req, res, function () {
        res.redirect("/secret");
        });
    });
});


// LOGIN ROUTES //

// get the login form
app.get("/login", function (req, res) {
    res.render("login")
})

// log the user in by passing in the authenticate method
// passport.authenticate is middleware and is executed before the actual route runs
app.post("/login", passport.authenticate("local", {
    // login in success redirect
    sucessRedirect: "/secret",
    // login failure redirect
    failureRedirect: "/login"
}), function(req, res) {
});

// log out
app.get("/logout", function(req,res) {
    // using passport to delete user data in the session to logout
    req.logout();
    res.redirect("/");
})
// secret can only be seen if logged in, eventually
app.get("/secret", isLoggedIn,function (req, res) {
    res.render("secret")
});

// middleware for checking login
// reqest, response, what's next
function isLoggedIn(req,res,next) {
    // use passport to check if the user is logged in
    if(req.isAuthenticated()) {
        // if logged in, run next(), which is the call back in the route that called this middleware
        return next();
    }
    // redirect to the login page if failed.
    res.redirect("/login")
}

// start the server
app.listen("3000", function () {
    console.log("Server is running");
});
