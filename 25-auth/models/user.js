
var mongoose              = require("mongoose"),
    passportLocalMongoose     = require("passport-local-mongoose");

var UserSchema = new mongoose.Schema({
username: String,
password: String
});

// takes passport-local-mongoose and applies those methods to the User Model
UserSchema.plugin(passportLocalMongoose)
module.exports = mongoose.model("User", UserSchema)
