// routes/comments.js

// routes related to comments

// Note about route paths: 
// +Since the app.use in app.js appends "/campgrounds/:id/comments" to the routes, 
// + the "/" index in the routes, is equivilent to "/campgrounds/:id/comments" ie: 
// +"/campgrounds/:id/comments/new" = "/new"

// init express
var express = require("express"),
    // use the express router rather than app
    // merge parameters from the comments and the campground routes
    router = express.Router({ mergeParams: true });

// import models
var Campground = require("../models/campground"),
    Comment = require("../models/comment");

// show comment form
// using isLoggedIn middleware to check if users are logged in
router.get("/new", isLoggedIn, function (req, res) {
    // find campground by id
    Campground.findById(req.params.id, function (err, campground) {
        if (err) {
            console.log(err)
        } else {
            res.render("comments/new", { campground: campground });
        }
    })
})

// submit the form
// using isLoggedIn middleware to make sure users are logged to create new comments
router.post("/", isLoggedIn, function (req, res) {
    //look up campground by ID
    Campground.findById(req.params.id, function (err, campground) {
        if (err) {
            console.log(err)
            res.redirect("/campgrounds")
        } else {
            // create a new comment
            Comment.create(req.body.comment, function (err, comment) {
                if (err) {
                    console.log(err)
                } else {
                    // get user id from the requst and assign to the comment model's author.id field
                    comment.author.id = req.user._id;
                    // get username from the requst and assign to the comment model's author.username field
                    comment.author.username = req.user.username
                    // save the comment with the updated fields from the request body
                    comment.save();
                    // connect new comment to campground
                    campground.comments.push(comment)
                    campground.save()
                    // redirect to show page
                    res.redirect("/campgrounds/" + campground._id)
                }
            })
        }
    })
});

//-- MIDDLEWARE --//

// checking if users are logged in
function isLoggedIn(req, res, next) {
    // check if the user is authenticated
    if (req.isAuthenticated()) {
        // if so, return out and keep moving through the rest of the callback
        return next();
    }
    // redirect to login page
    res.redirect("/login");
};

// export the router object and the attached routes
module.exports = router;