var mongoose = require('mongoose');

// connect to the db and use the admin db for auth.
mongoose.connect('mongodb://root:root@localhost:8081/yelpcamp?authSource=admin', { useNewUrlParser: true });


// Campground Structure
var campgroundSchema = new mongoose.Schema({
    name: String,
    image: String,
});

// set Campground type equal to mongoose campgroundSchema structure
// ('Campground', ...) is the name of the collection in the DB
var Camp = mongoose.model('Campground', campgroundSchema);

// stub data to the db
var site = new Camp({ name: "Salmon Creek", image: "https://www.nps.gov/havo/planyourvisit/images/Namakanipaio_960.jpg?maxwidth=1200&maxheight=1200&autorotate=false" })

/*
// save to db
//save methods require the variable. 
site.save(function(err, site) {
    if(err) {
        console.log("Didn't save to DB");
    } else {
        console.log("Saved to the db:");
        console.log(site);
    }
});
*/
/*
// create and save to the db
Camp.create({
    name: "New Site",
    image: "http://something.com"
}, function(err, site) {
    if(err) {
        console.log("error")
    } else {
        console.log("yay")
    }

});

// delete anything with a certain image string.
Camp.deleteMany({image: "http://something.com"}, function(err, site) {
    if(err) {
        console.log(" ")
    } else {
        console.log(" ")
    }
});
*/
// find methods require the collection to search through. 
Camp.find({}, function(err, site) {
    if(err) {
        console.log("These aren't the droids you're looking for.");
    } else {
        console.log("Found this: ");
        console.log(site);
    }
});