// Node Exercise 2
// Average Grader

// define array of test scores
var scores = [90, 98, 89, 100, 100, 86, 94];
var scores2 = [40, 65, 77, 82, 80, 54, 73, 63, 95, 49]

// define function named "average" that takes the array as an argument
function average(scores) {
    // init total var
    var total = 0;
    // init avg var 
    var avg = 0;
    // loop over array and sum the numbers
    for(var i = 0; i < scores.length; i++ ) {
        total += scores[i];
    }
    // divide sum by length to get average
    avg = total / scores.length;

    // round average to the nearest whole number and return
    return Math.round(avg);

}

// call function and feed in arguement

console.log(average(scores));
console.log(average(scores2));