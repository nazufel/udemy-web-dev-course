var age = prompt("How old are you?");

// if age is negative
if(age < 0) {
    console.log("Sorry, you gave a negative number.");
} 

// if age is 21
else if(age === 21) {
    console.log("Happy drinking year!")
}

// if age is odd
else if(age % 2 !== 0) {
    console.log("Your age is an odd number.")
}

// if age is a perfect square
// had to lookup Math.sqrt
else if(Math.sqrt(age) % 1 === 0) {
    console.log("Your age is a perfect square.")
}