var mongoose = require("mongoose")

// DB AND MODEL //

// connect to the db
mongoose.connect('mongodb://root:root@localhost:8081/dataassciation?authSource=admin', { useNewUrlParser: true });


// POST //
// define post schema
var postSchema = new mongoose.Schema({
    title: String,
    content: String
})

// init model
var Post = mongoose.model("Post", postSchema)

// USER //
// define user model
var userSchema = new mongoose.Schema({
    email: String,
    name: String,
    // embedding the postSchema into user to make data association
    // embedded data must come after such data is defined
    posts: [postSchema]
})

// init model
var User = mongoose.model("User", userSchema)

/*
// defining new user
var newUser = new User({
    email: "hermione@hogwarts.edu",
    name: "Hermione Granger"
})

// defining new post and pushing into posts array
newUser.posts.push({
    title: "How to brew polyjuice potion",
    content: "Just kidding. Go to potions class."
})

// saving to the db
newUser.save(function(err, user) {
    if(err)  {
        console.log(err)
    } else {
        console.log(user)
    }
})

*/

/*
var newPost = new Post({
    title: "Reflections on Apples",
    content: "They are delicious"
})

newPost.save(function(err, post) {
    if(err) {
        console.log(err)
    } else {
        console.log(post)
    }
})
*/

User.findOne({name: "Hermione Granger"}, function(err, user) {
    if(err) {
        console.log(err)
    } else {
        user.posts.push({
            title: "Three things i really hate",
            content: "Voldemort. Voldemort. Voldemort."
        })
        user.save(function(err, user) {
            if(err) {
                console.log(err) 
            } else {
                console.log(user)
            }
        })
    }
})