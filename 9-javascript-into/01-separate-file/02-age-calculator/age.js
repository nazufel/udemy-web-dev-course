var years = prompt("How old are you?");

// the .25 is an average to account for leap years as they come ever four years
var days = years * 365.25;

alert("You are " + days + " days old.");