var button = document.querySelector("button");

var isPurple = false;

/* commented out for refactor
button.addEventListener("click", function(){
    // if white
    if(isPurple) {
        // make it white
        document.body.style.background = "white";
    } else {
        // make it purple
        document.body.style.background = "purple";       
    }
    isPurple = !isPurple;   
});

*/

// event listener for clicks
button.addEventListener("click", function() {
    // toggle on the purple css class.
    document.body.classList.toggle("purple");
});