# Notes About CSS

## General Rule for CSS

### Structure
```css
selector {
    property: value;
    anotherProperty: value;
}
```

### Example

```css

/* Give all h1's purple and 56px font*/
h1 {
    color: purple;
    font-size: 56px;
}

/* Give all img's a 3px red border*/
img {
    border-color: red;
    border-width: 3px;
}

```
