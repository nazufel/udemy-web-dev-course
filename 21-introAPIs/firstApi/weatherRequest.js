// firstRequest.js

/*
// request
const request = require('request');
                                                                                // create arrow function, same as callback.
request('https://jsonplaceholder.typicode.com/users/1', (error, response, body) => {
    // freezes code at this spot. looks like ipython. 
    //eval(require('locus'))

    // check the error
    if (!error &&  response.statusCode == 200) {
        // const is for variables that won't change.
        const parsedData = JSON.parse(body);
        // print only the bs in the deep nested structure of the json.
        // new ES6 template litteral syntax with backticks and variable brackets.
        console.log(`${parsedData.name} lives in ${parsedData.address.city}`);
    };

});
*/

// request-promise
const rp = require('request-promise');

// use request-promise to call to api
rp('https://jsonplaceholder.typicode.com/users/1')

    // take the body and parse into json
    .then((body) => {
        const parsedData = JSON.parse(body);
        // print the string with the user's name lives in the address city
        console.log(`${parsedData.name} lives in ${parsedData.address.city}`);
    })

    // catch the error if any
    .catch((err) => {
        console.log('Error!', err);
    });