// routes/index.js

// routes not related to any models

// init express and router
var express = require("express"),
    // use the express router rather than app
    router = express.Router();

// init passport
var passport = require("passport");

// import models
var User = require("../models/user");

//-- ROUTES --//

// Landing Page
router.get('/', function (req, res) {
    res.render("landing");
});


// auth routes //

// show register form
router.get("/register", function(req,res) {
    res.render("register");
});

// singup
router.post("/register", function(req,res) {
    // grab the username from the form and make a new user object
    var newUser = new User({username: req.body.username})
    // session information and save to the db
    User.register(newUser, req.body.password, function(err, user) {
        // hangle errors
        if(err) {
            console.log(err)
            return res.render("register")
        } 
        // use the local strategy to sign in and redirect to campgrounds
        passport.authenticate("local")(req,res,function() {
            res.redirect("/campgrounds")
        })
    }) 
})

// show login form
router.get("/login", function(req,res) {
    // render the form
    res.render("login")
})

// login logic
// Colt's code didn't work. copied this from http://www.passportjs.org/packages/passport-local/
router.post('/login', 
    // use passport's local strategy. failures redirect to /login
    passport.authenticate('local', { failureRedirect: '/login' }),
    function(req, res) {
        // success redirect back to campgrounds
        res.redirect('/campgrounds');
});

// logout logic
router.get("/logout", function(req,res) {
    // logout using the builtin method provided by passport
    req.logout();
    // redirect to campgrounds
    res.redirect("/campgrounds");
})

//-- MIDDLEWARE --//

// checking if users are logged in
function isLoggedIn(req, res, next) {
    // check if the user is authenticated
    if(req.isAuthenticated()) {
        // if so, return out and keep moving through the rest of the callback
        return next();
    }
    // redirect to login page
    res.redirect("/login");
};

// export the router object and the attached routes
module.exports = router;