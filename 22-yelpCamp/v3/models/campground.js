// models/campground.js

// define the campground model //

// require mongoose
var mongoose = require("mongoose");

// define campground schema with embedded comments
var campgroundSchema = new mongoose.Schema({
    name: String,
    image: String,
    description: String,
    // comment property is an object
    comments: [
        {
            // referencing comments by ObjectId
            type: mongoose.Schema.Types.ObjectId,
            // use the Comment model
            ref: "Comment"
        }
    ]
});

// init and export campground model
module.exports = mongoose.model("Campground", campgroundSchema);

