// POST //

var mongoose = require("mongoose");

// define post schema
var postSchema = new mongoose.Schema({
    title: String,
    content: String
})

// init model and export
module.exports = mongoose.model("Post", postSchema)

 