var numSquares = 6;
var colors = generateRandomcolors(6);
var squares = document.querySelectorAll(".square");
var pickedColor = pickColor();
var colorDisplay = document.getElementById("colorDisplay");
var messageDisplay = document.querySelector("#message");
var h1 = document.querySelector("h1");
var resetButton = document.querySelector("#reset");
var modeButtons = document.querySelector(".mode");

for(var i = o; i < modeButtons.length; i++) {
    modeButtons[i].addEventListener("click", function() {
        modeButtons[0].classList.remove("selected")
        modeButtons[1].classList.remove("selected")
        this.classList.add("selected")

        
        // figure out how many squares to show
        // Turnary Operator example
        // this.textContent === "Easy" ? numSquares = 3; numSquares = 6;
        if(this.textContent === "Easy") {
            numSquares = 3;
        } else {
            numSquares = 6;
        }
        reset();
        // pick new colors

        // pickk a new pickedColor

        // update page
    });
};

function reset() {
     // generate all new colors
     colors = generateRandomcolors(numSquares);
     // pick a new random color from array
     pickedColor = pickColor();
     // change the colros of the square on the page
     colorDisplay.textContent = pickedColor;
     messageDisplay.textContent = "";
     resetButton.textContent = "New Colors";
     // reset square colors
     for(var i = 0; i < squares.length; i++) {
         if(colors[i]) {
            // add initial colors to squares
            squares[i].style.backgroundColor = colors[i];
         } else {
             squares[i].style.Display = "none";
         }

     };
     h1.style.backgroundColor = "steelblue";
}

/*
easyBtn.addEventListener("click", function() {
    hardBtn.classList.remove("selected");
    easyBtn.classList.add("selected");
    // pick a new random color from array
    numSquares = 3;
    colors = generateRandomcolors(numSquares);
    pickedColor = pickColor();
    // change the colros of the square on the page
    colorDisplay.textContent = pickedColor;
    for(var i = 0; i < squares.length; i++) {
        if(colors[i]) {
            squares[i].style.backgroundColor = colors[i];
        } else {
            squares[i].style.display = "none";
        };
    };    

});

hardBtn.addEventListener("click", function() {
    hardBtn.classList.add("selected");
    easyBtn.classList.remove("selected");
    // pick a new random color from array
    colors = generateRandomcolors(6);
    pickedColor = pickColor();
    // change the colros of the square on the page
    colorDisplay.textContent = pickedColor;
    for(var i = 0; i < squares.length; i++) {
            squares[i].style.backgroundColor = colors[i];
            squares[i].style.display = "block";
        };
});

*/
resetButton.addEventListener("click", function() {
    // generate all new colors
    colors = generateRandomcolors(numSquares);
    // pick a new random color from array
    pickedColor = pickColor();
    // change the colros of the square on the page
    colorDisplay.textContent = pickedColor;
    messageDisplay.textContent = "";
    this.textContent = "New Colors";
    // reset square colors
    for(var i = 0; i < squares.length; i++) {
        // add initial colors to squares
        squares[i].style.backgroundColor = colors[i];
    };
    h1.style.backgroundColor = "steelblue";
});


colorDisplay.textContent = pickedColor;

// game loop
for(var i = 0; i < squares.length; i++) {
    // add initial colors to squares
    squares[i].style.backgroundColor = colors[i];

    // add click listners to squres
    squares[i].addEventListener("click", function() {
    
    // grab color of picked square
    var clickedColor = this.style.backgroundColor;
    // compare color to pickedColor
    if(clickedColor === pickedColor) {
        messageDisplay.textContent = "Correct!";
        changeColors(clickedColor);
        h1.style.backgroundColor = clickedColor;
        resetButton.textContent = "Play Again?";
    } else {
        this.style.backgroundColor = "#232323";
        messageDisplay.textContent = "Try Again."
    };
    });
};

function changeColors(color) {
    // loop through all squares
    for(var i = 0; i < squares.length; i++) {
        squares[i].style.backgroundColor = color;
    };
    // change color color to match given color
};

function pickColor() {
    // Math.floor chops off decimals
    var random = Math.floor(Math.random() * colors.length);
    return colors[random];
};

function generateRandomcolors(num) {
    // make an array
    var arr = []
    // add num random colors to array
    for(var i = 0; i < num; i++) {
        // get random color and push ingo array
        arr.push(randomColor());
    };
    // return array at the end
    return arr
}

function randomColor() {
    // pick a red 0 to 255
    var r = Math.floor(Math.random() * 256);
    // pick a green 0 to 255
    var g = Math.floor(Math.random() * 256);
    // pick a blue 0 to 255
    var b = Math.floor(Math.random() * 256);
    return "rgb(" + r + ", " + g + ", " + b +")";
}
