// yelpcamp/v1/app.js

// init packages and set defaults //

// import express
var express = require("express");
    // import body-parser
var bodyParser = require('body-parser');

// import mongoose
mongoose = require('mongoose');

// init express
var app = express();

// init body-parser
app.use(bodyParser.urlencoded({ extended: true }));

// set ejs as render engine
app.set('view engine', 'ejs');

// Database stuff //

// connect to the db
mongoose.connect('mongodb://root:root@localhost:8081/yelpcamp?authSource=admin', { useNewUrlParser: true });

// Campground Structure
var campgroundSchema = new mongoose.Schema({
    name: String,
    image: String,
    description: String
}, {
    versionKey: false // don't want "__v":0 at then end of each document
});

// set Campground type equal to mongoose campgroundSchema structure
// ('Campground', ...) is the name of the collection in the DB
var Campground = mongoose.model('Campground', campgroundSchema);

// Routes //

// Landing Page
app.get('/', function (req, res) {
    res.render("landing");
});

// INDEX - Get all Campgrounds
app.get('/campgrounds', function (req, res) {
    // find methods require the collection to search through. 
    Campground.find({}, function (err, campgrounds) {
        if (err) {
            console.log("These aren't the droids you're looking for.");
        } else {
            // render the campgrounds template with campgrounds data
            res.render('index', { campgrounds: campgrounds });
        }
    });
});

// NEW - Display creation form
app.get('/campgrounds/new', function (req, res) {
    // show new campground form.
    res.render('newcampground')
})

// CREATE - Add a campground
app.post("/campgrounds", function (req, res) {
    console.log("hit the post route")
    // get data from the form and add to campgrounds array
    var name = req.body.name;
    var image = req.body.image;
    var desc = req.body.description;
    // make an object from request body fields to be created in the db 
    var newCampground = { name: name, image: image, description: desc };
    // create a new campground and save to db
    Campground.create(newCampground, function (err, newCampground) {
        // check the error
        if (err) {
            console.log("error");
            // log the object and redirect
        } else {
            console.log("Create successful:");
            // redirect back to campgrounds page
            res.redirect("/campgrounds");
        }
    });
});

// SHOW - shows one particular campground
// note, the url param /:id could be anything, thus must be last as
//+ /campgrounds/new could be processed as an ID by the router, rather than a route
app.get("/campgrounds/:id", function(req,res) {
    // grab the id
    
    Campground.findById(req.params.id, function(err, foundCampground) {
        if(err) {
            console.log(err);
        } else {
            // render the template and pass in the data
            res.render("show", {campground: foundCampground});
        }
    });
});

// start the server and listen
app.listen('8000', function () {
    console.log("Server started.");
});
