// routes/campgrounds.js

// Note about route paths: 
// +Since the app.use in app.js appends "/campgrounds" to the routes, 
// + the "/" index in the routes, is equivilent to "/campgrounds" ie: 
// +"/campgrounds/new" = "/new"


// init express
var express = require("express"),
    // use the express router rather than app
    router = express.Router();

// import models
var Campground = require("../models/campground"),
    Comment = require("../models/comment");

// INDEX - Get all Campgrounds
router.get('/', function (req, res) {
    // get logged in user info
    // find methods require the collection to search through. 
    Campground.find({}, function (err, campgrounds) {
        if (err) {
            console.log("These aren't the droids you're looking for.");
        } else {
            // render the campgrounds template with campgrounds and user data via req.locals.currentUser middleware
            res.render('campgrounds/index', { campgrounds: campgrounds });
        }
    });
});

// NEW - Display creation form
// using isLoggedIn middleware to make sure users are logged to create new campgrounds
router.get('/new', isLoggedIn, function (req, res) {
    // show new campground form.
    res.render('campgrounds/newcampground')
})

// CREATE - Add a campground
// using isLoggedIn middleware to make sure users are logged to create new campgrounds
router.post("/", isLoggedIn, function (req, res) {
    console.log("hit the post route")
    // get data from the form and add to campgrounds array
    var name = req.body.name;
    var image = req.body.image;
    var desc = req.body.description;
    var author = {
        id: req.user._id,
        username: req.user.username
    }
    // make an object from request body fields to be created in the db 
    var newCampground = { name: name, image: image, description: desc, author: author };
    // create a new campground and save to db
    Campground.create(newCampground, function (err, newCampground) {
        // check the error
        if (err) {
            console.log("error");
            // log the object and redirect
        } else {
            console.log("Create successful:");
            // redirect back to campgrounds page
            res.redirect("/campgrounds");
        }
    });
});

// SHOW/READ - shows one particular campground
// note, the url param /:id could be anything, thus must be last as
//+ /campgrounds/new could be processed as an ID by the router, rather than a route
router.get("/:id", function (req, res) {
    // grab the id, retrive the associated comments, and execute the rest of the call back.
    Campground.findById(req.params.id).populate("comments").exec(function (err, foundCampground) {
        if (err) {
            console.log(err);
        } else {
            console.log(foundCampground)
            // render the template and pass in the data
            res.render("campgrounds/show", { campground: foundCampground });
        }
    });
});

// UPDATE - updates a campground
// show the update form
router.get("/:id/edit", checkCampgroundOwnership, function (req, res) {
    // run middlewhare chackCampgroundOwnership...
    // find campground
        Campground.findById(req.params.id, function (err, foundCampground) {
                // render the form with the campground data
                res.render("campgrounds/edit", { campground: foundCampground });
        });
});

// write the changes to db
router.put("/:id", checkCampgroundOwnership, function (req, res) {
    // find and update the correct campground
    Campground.findByIdAndUpdate(req.params.id, req.body.campground, function (err, updatedCampground) {
        // handle the error by redirecting to campgrounds
        if (err) {
            res.redirect("/campgrounds");
        }
        // redirect back to that campground that was updated
        res.redirect("/campgrounds/" + req.params.id)
    })

})

// DELETE - deletes campground
// destroy the campground
router.delete("/:id", checkCampgroundOwnership, function (req, res) {
    // look up the campground by the ID and delete
    Campground.findByIdAndDelete(req.params.id, function (err, deletedCampground) {
        // handle the error
        if (err) {
            res.redirect("/campgrounds")
        }
        // delete comments associated with the campground
        Comment.deleteMany({ _id: { $in: deletedCampground.comments } }, function (err) {
            // handle the error
            if (err) {
                console.log(err);
                // rediect if failure
                res.redirect("/campgrounds");
            }
        });
        // otherwise redirect to campgrounds
        res.redirect("/campgrounds")
    })
});

//-- MIDDLEWARE --//

// checking if users are logged in
function isLoggedIn(req, res, next) {
    // check if the user is authenticated
    if (req.isAuthenticated()) {
        // if so, return out and keep moving through the rest of the callback
        return next();
    }
    // redirect to login page
    res.redirect("/login");
};

// check campground ownership
function checkCampgroundOwnership(req,res,next) {
    if (req.isAuthenticated()) {
        Campground.findById(req.params.id, function (err, foundCampground) {
            // handle the error and redirect
            if (err) {
                res.redirect("back");
            }
            // does user own the campground
            // equals method provided by mongoose to compare mongoose object to string
            if(foundCampground.author.id.equals(req.user._id)) {
                // otherwise proceed with the rest of the function
                next();
            } else {
                res.redirect("back")
            }
        });
    } else {
        // send them back where they came from.
        res.redirect("back");
    }
    // if not, redirect
    // if not, redirect
    // serach for campground by id from the request url
};

// export the router object with the routes attached
module.exports = router;