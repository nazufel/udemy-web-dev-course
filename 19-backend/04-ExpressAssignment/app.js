// app.js - Express assignment

// 1. Create a new Express app from scratch
// 2. Create a package.json using `npm init` and add express as a dependency
// 3. In the main app.ja file, add different routes:
    // 1. "/" should print, "Hi there, welcome to my assignment!"
    // 2. "/speak/pig" should print "oink"
    // "/speak/cow" should print "moo"
    // "/speak/dog" should print "woof"
    // 3. "/repeat/hello/3" should print the number of hellos
    // 4. "*" Sorry, page not found... What are you doing with your life?

// PROCESS //

// import express and init
var express = require("express");

var app = express();

// ROUTES // 

// get "/" print string
app.get("/", function(req,res) {
    res.send("Hi there, welcome to my assignment!");
})

// get "/speak/:"
app.get("/speak/:animal", function(req, res) {
    // create sounds object with animals and their sounds
    sounds = {
        "pig": "Oink",
        "cow": "Moo",
        "dog": "Woof",
    }

    var animal = req.params.animal.toLowerCase()
    var sound = sounds[animal]

    res.send("The " + animal + " says, " + sound + "!");
})

// get "/repeat/:/:"
app.get("/repeat/:phrase/:num", function(req,res) {
    // assign the parts of the params to variables
    var phrase = req.params.phrase
    var num = req.params.num 
    var response = ""

    // convert number var into an int from string
    var num = Number(num)

    // for loop to print phrase n number of times
    for(var i = 0; i < num; i++) {
        response += phrase + " "
    }
    res.send(response)
})


// get "*" print "Sorry, page not found... What are you doing with your life?"
app.get("*", function(req,res) {
    res.send("Sorry, page not found... What are you doing with your life?")
})

app.listen(8000, function() {
    console.log("app.js is running");
})