

/* 
for(init; condition; step) {
    // run code
}
*/

// print all numbers between -10 and 19 with a for loop
console.log("print all numbers between -10 and 19 with a for loop")
for (var num = -10; num < 19; num++) {
    console.log(num);
}

// print all even numbers between 10 and 40
console.log("print all even numbers between 10 and 40 with a for loop")
for (var num = 10; num < 40; num++) {
    if (num % 2 == 0) {
        console.log(num);
    }
}

// print all odd numbers between 300 and 333
console.log("print all odd numbers between 300 and 333 with a for loop")

for (var num = 300; num <= 333; num++) {
    if (num % 2 !== 0) {
        console.log(num);
    }
}

// print all numbers divisible by 5 and 3 that are between 5 and 50
console.log("print all numbers divisible by 5 and 3 that are between 5 and 50 with a for loop")

for (var num = 5; num <= 50; num++) {
    if (num % 5 == 0 && num % 3 == 0) {
        console.log(num);
    }
}