
var sound = new Howl({
    src: ['sounds/bubbles.mp3']
  });

// Create a Paper.js Path to draw a line into it:
// init circles array
var circles = [];

function onKeyDown(event) {
    sound.play();
    var maxPoint = new Point(view.size.width, view.size.height);
    // Point.random() generates a number less than 1. That way,
    //+ multiplying maxPoint by randomPoint will always result
    //+ in a number less than the maxPoint. 
    var randomPoint = Point.random();
    var point = maxPoint * randomPoint;
    // when key is pressed, create a new circle
    var newCircle = new Path.Circle(point, 500);
    // starting fill color
    newCircle.fillColor = 'orange';
    // push circle to array
    circles.push(newCircle);
};

function onFrame(event) {
    // for every circle do the following
    for(var i = 0; i < circles.length; i++ ) {
        // change the color hue with every frame
        circles[i].fillColor.hue += 1;
        // change the size with every frame
        circles[i].scale(.9);
    };
};