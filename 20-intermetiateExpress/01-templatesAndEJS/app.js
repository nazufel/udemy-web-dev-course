var express = require("express");
var app = express();

// tells express to serve up the public directory where styles and js live.
app.use(express.static("public"))

// expect template files to be ejs.
app.set("view engine", "ejs");


app.get("/", function(req, res) {
    // need to npm install ejs for this to work, but doesn't need to be imported into the app.
    // no need for absolute path to "views/". ejs looks in views by default.
    res.render("index");
});

app.get("/love/:thing", function(req,res) {
    var thing = req.params.thing;
    // first "thing" var is the variable in the template, then assigning route "thing" to template "thing".
    res.render("love", {thing: thing});
});

app.get("/posts", function(req,res) {
    var posts = [
        {title: "Post 1", author: "Me"},
        {title: "Post 2", author: "Me"},
        {title: "Post 3", author: "Me"}
    ]

    res.render("posts", {posts: posts});

})

app.listen("8000", function() {
    console.log("app.js is running.");
});