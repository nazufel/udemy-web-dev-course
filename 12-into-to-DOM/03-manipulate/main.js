/*
Example of manipulation

// Select
var tag = document.getElementById("highlight");

// Manipulate
tag.style.color = "blue";
tag.style.border = "10px solid red";
tag.style.fontSize = "70px";
tag.style.background = "yellow";
tab.style.marginTop = "200px";

*/

// turn on the .big class

// long way to do it
var p = document.querySelector("p")
p.classList.add("big")

// oneliner
document.querySelector("p").classList.add("big")

/*
This is a better way to dynamically style. Have prebuilt selectors in CSS and
have js turn them on and off based on client behavior. js shouldn't be doing
the direct
*/ 

