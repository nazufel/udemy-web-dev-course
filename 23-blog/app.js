// 23-blog/app.js

// INIT PACKAGES //
var bodyParser = require("body-parser"),
    express = require("express"),
    mongoose = require("mongoose"),
    methodOverride = require("method-override");
    expressSanitizer = require("express-sanitizer")
// init express
var app = express();


// DB AND MODEL //

// connect to the db
mongoose.connect('mongodb://root:root@localhost:8081/restfulblog?authSource=admin', { useNewUrlParser: true });

// mongoose model config
var blogSchema = new mongoose.Schema({
    title: String,
    image: String,
    body:  String,
    created: {type: Date, default: Date.now}
}, {
    versionKey: false // don't want "__v":0 at then end of each document
});

// init the mongoose model
var Blog = mongoose.model("Blog", blogSchema);

// SET/USE DEFAULTS //

app.set("view engine", "ejs");
app.use(express.static("public"));
app.use(bodyParser.urlencoded({extended: true}));
// whenever the app gets an _method=<method>, treat that method like the one overridden
app.use(methodOverride("_method"));
// only caviat is that the sanitizer must be used after ejs is set.
app.use(expressSanitizer());


// ROUTES //

// INDEX //

// redirect to "/blogs"
app.get("/", function(req,res) {
    res.redirect("/blogs")
});

// Index of blogs, get them all
app.get("/blogs", function(req,res) {
    Blog.find({}, function(err, blogs) {
        if(err) {
            console.log(err);
        } else {
            res.render("index", {blogs: blogs});
        }
    });
});

// NEW //

// show the form for creating a new post
app.get("/blogs/new", function(req,res) {
    // render the form for adding new blog post
    res.render("new");
});

// post the form data to the DB
app.post("/blogs", function(req,res) {
    // sanitize the body input
    req.body.blog.body = req.sanitize(req.body.blog.body)
    // create the blog
    Blog.create(req.body.blog, function(err, newBlog) {
        // check the error
        if(err) {
            res.render("new");
        } else {
            // redirect to index
            res.redirect("/blogs")
        }
    })
});

// SHOW //

// show a single blog post
app.get("/blogs/:id", function(req,res) {
    Blog.findById(req.params.id, function(err, foundBlog) {
        if(err) {
            res.redirect("/blogs");
        } else {
            res.render("show", {blog: foundBlog});
        }
    })
})

// EDIT //

// edit existing blog entry
app.get("/blogs/:id/edit", function(req,res) {
    Blog.findById(req.params.id, function(err, foundBlog) {
        if(err) {
            res.redirect("/blogs");
        } else {
            res.render("edit", {blog: foundBlog});
        }
    })
})

// UPDATE //

// update an existing blog entry
app.put("/blogs/:id", function(req,res) {
    // sanitize the body input
    req.body.blog.body = req.sanitize(req.body.blog.body)
    // create the blog
    Blog.findByIdAndUpdate(req.params.id, req.body.blog, function(err,updatedBlog) {
        if(err) {
            res.redirect("/blogs")
        } else {
            res.redirect("/blogs/" + req.params.id)
        }
    })
})

// DELETE //

// delete an existing blog entry
app.delete("/blogs/:id", function(req,res) {
    Blog.findByIdAndRemove(req.params.id, function(err) {
        if(err) {
            res.redirect("/blogs")
        } else {
            res.redirect("/blogs")
        }
    })
})

// start the server
app.listen("8000", function() {
    console.log("Server is running");
});
