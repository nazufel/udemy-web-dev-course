// listProducts.js

// Use Faker to print out 10 random product names and prices 

var faker = require("faker");

// PROCESS

// Print welcome to the shop

console.log("====================================");
console.log("Khajiit has wares, if you have coin.");
console.log("====================================");

// for loop 10 times 
for(var i = 0; i < 10; i++) {
    // console.log fake color, name, and price
    console.log(faker.fake("{{commerce.productName}}") + " " + "-" + " " + "$" + faker.fake("{{commerce.price}}")); 
}

