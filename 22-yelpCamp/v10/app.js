// yelpcamp/v*/app.js

// import packages
var express = require("express"),
    // init express must be before anything else
    app = express(),
    bodyParser = require('body-parser'),
    LocalStrategy = require("passport-local"),
    methodOverride = require("method-override"),
    passport = require('passport'),
    seedDB = require("./seeds");

// import routes
var commentRoutes = require("./routes/comments"),
    campgroundRoutes = require("./routes/campgrounds"),
    indexRoutes = require("./routes/index");

// import models
var User = require("./models/user"),
    Campground = require("./models/campground"),
    Comment = require("./models/comment");

// import mongoose
mongoose = require('mongoose');

//-- init packages and set defaults --//

// init body-parser
app.use(bodyParser.urlencoded({ extended: true }));

// use method override to allow PUT and DELETE in forms when ?_method=<method> in URL
app.use(methodOverride("_method"));

// passport config //
app.use(require("express-session")({
    secret: "Expecto Potronum",
    resave: false,
    saveUninitialized: false
}));
app.use(passport.initialize());
app.use(passport.session());

// middleware to pass logged in user to every route
app.use(function (req, res, next) {
    // whatever is in res.locals is available to the templates
    res.locals.currentUser = req.user;
    // do the next thing in the calling function/handler
    next();
});

passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

// set ejs as render engine
app.set('view engine', 'ejs');

// use the custom files in the public directory
app.use(express.static(__dirname + "/public/"))

// Database stuff //

// connect to the db
mongoose.connect('mongodb://root:root@localhost:8081/yelpcamp?authSource=admin', { useNewUrlParser: true });

// seed the DB
seedDB();

// use the route files
app.use(indexRoutes);
// adds "/campgrounds" to the routes; thus, the actual routes "/" is "/campgrounds"
app.use("/campgrounds", campgroundRoutes);
// adds "/campgrounds/:id/comments" to the routes; thus, the actual routes "/" is "/campgrounds/:id/comments"
app.use("/campgrounds/:id/comments", commentRoutes);

// start the server and listen
app.listen('8000', function () {
    console.log("Server started.");
});
