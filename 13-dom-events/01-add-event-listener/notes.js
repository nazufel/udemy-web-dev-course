var button = document.querySelector("button");

button.addEventListener("click", function(){ 
    console.log("SOMEONE CLICKED THE BUTTON!");
});

// select LI's and put them in a variable
var lis = document.querySelectorAll("li");

// loop over the LI's and when any are clicked, change to pink
for(var i = 0; i < lis.length; i++) {
    lis[i].addEventListener("click", function() {
        this.style.color = "pink";
    });
};