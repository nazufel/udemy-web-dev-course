# Data Associations

## Two Types of Data Association
1. Embedded
2. Reference


## Embeded Data

Create multiple models and then have an array of one model inside of another one:

```js
// POST //
// define post schema
var postSchema = new mongoose.Schema({
    title: String,
    content: String
})

// USER //
// define user model
var userSchema = new mongoose.Schema({
    email: String,
    name: String,
    // embedding the postSchema into user to make data association
    // embedded data must come after such data is defined. 
    // since post is embedded into user, the post schema must come first in the file
    posts: [postSchema]
})

```

The user model here has an array of posts defined. Adding in new posts into the user model requires a push. The process is as follows

```js
// find the user, in this case, Hermione is statically added, but this could also be a variable
User.findOne({name: "Hermione Granger"}, function(err, user) {
    if(err) {
        console.log(err)
    } else {
        // push the post onto posts schema for that user
        user.posts.push({
            title: "Three things i really hate",
            content: "Voldemort. Voldemort. Voldemort."
        })
        // save user and pushed post back to the database
        user.save(function(err, user) {
            if(err) {
                console.log(err) 
            } else {
                console.log(user)
            }
        })
    }
})
```
## Referenced Data

Reference data has the id of the object being referenced rather than the whole object being embedded.
```js
// reference object
{
    "username": "Hermione Granger",
    "posts": [ 
        "id": id("1234"),
        "id": id("4567"),
        "id": id("91011")
}

//referenced objects
{
    "id": id("1234"),
    "title": "How to Make Polyjuice Potion",
    "content": "The book starts with..."
},
{
    "id": id("5678"),
    "title": "How to make things levitate",
    "content": "The book starts with..."
},
{
    "id": id("91011"),
    "title": "How to Fight the One Who Must not Be Named",
    "content": "The book starts with..."
}
```

The referenced data lives outside of the embedded references.