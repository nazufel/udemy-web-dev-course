# list of jQuery methods

* val()
* text()
  * Strips out html elements and gives straight text
  * sub for ```h1.textContent = "New Text"```
  
        $("h1").text("New Text")
* attr()
    * Short for attribute
    * Alter specific attributes
* html()
   * Gets the text and html elements
* addClass()
* removeClass()
* toggleClass()