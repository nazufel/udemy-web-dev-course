// firstRequest.js

// require 
const request = require('request');
request('http://www.google.com', function (error, response, body) {

    // check the error
    if (error) {
        // print the error
        console.log('error:', error); // Print the error if one occurred
        // print the status code
        console.log('statusCode:', response && response.statusCode); // Print the response status code if a response was received

    };

    // check the status code, if 200, print the body
    if (response.statusCode == 200) {

        // print the body html.
        console.log('body:', body); // Print the HTML for the Google homepage.
    };

});
