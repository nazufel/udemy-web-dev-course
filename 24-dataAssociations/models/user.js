// USER //

var mongoose = require("mongoose");

// define user model
var userSchema = new mongoose.Schema({
    email: String,
    name: String,
    // the posts 
    posts: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: "Post"
    }]
})

// init model and export
module.exports = mongoose.model("User", userSchema)