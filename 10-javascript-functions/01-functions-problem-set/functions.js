// global declaration of the var

// isEven() takes a number and returns true or false if even
function isEven(num) {
    if (num % 2 === 0) {
        return true
    } else {
        return false
    }
}

//refactored
function isEven2(num) {
    return num % 2 === 0
}
/*
// factorial() takes a number  and returns the factoral of that number
function factorial(num) {
    // define a result var
    var result = 1;
    // calculate factorial and store value in result
    for(var i = 2; i <= num; i++) {
        result *= i;
    }
    // return the result var
    return result;
} ​
*/
// kababToSnake() turns kababcase into snakecase
function kababToSnake(str) {
    //replace all dashes '-' with underscores '_'
    var newStr = str.replace(/-/g, "_");
    //return str
    return newStr;
}