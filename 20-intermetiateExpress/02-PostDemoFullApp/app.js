// 02-PostDemoFullApp/app.js

// init vars and packages //

// require express
var express = require("express");
    // require body-parser
    bodyParser = require("body-parser");

// init express's methods
var app = express();

// set the rendering engine as ejs
// ejs must be installed with npm and can be saved to package.json, but doesn't have to be explicitly called
app.set("view engine", "ejs");

// init the body-parser package
app.use(bodyParser.urlencoded({extended: true}));

// init friends data 
var friends = ["Allen", "Anfernee", "Leia", "Nick"]

// App Routes //

// Home page
app.get("/", function(req,res) {
    // render the homepage in views/home.ejs
    res.render("home");
});

// "/friends" endpoint, trying to follow REST principles in CRUD

// C - Create friends
app.post("/friends", function(req,res){
    // grab the request form body and assign to a variable
    var name = req.body;

    // push the name to the friends array
    friends.push(name.name);

    // redirect and do a get on the "/friends" enpoint to render all the friends with the new one. 
    res.redirect("/friends");
});

// R - Read friends
app.get("/friends", function(req,res) {
    // render the "views/fiends.ejs" template and pass in the friends array.
    res.render("friends", {friends: friends});
});

// star the server and listen on port 8000
app.listen("8000", function() {
    // console that the serer is running.
    console.log("Server is running.")
});