// models/comment.js

// define the comment model // 

// require mongoose
var mongoose = require("mongoose");
     
// set the comment schema
var commentSchema = new mongoose.Schema({
    text: String,
    // right now, author is a string, but will reference the author model when user auth is implimented.
    author: String
});
 
// init and export the model
module.exports = mongoose.model("Comment", commentSchema);


