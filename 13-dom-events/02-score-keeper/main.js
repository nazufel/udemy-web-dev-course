
// select the buttons
var p1Button = document.getElementById("p1Button");
var p2Button = document.getElementById("p2Button");
var resetButton = document.getElementById("reset");
// grabbing by CSS id=p*Display
var p1Display = document.querySelector("#p1Display");
var p2Display = document.querySelector("#p2Display");
var h1 = document.querySelector("h1 span");

var numInput = document.querySelector("input");
// select spans inside of paragraphs
var winningScoreDisplay = document.querySelector("p span")

// set the scores at the beginning of a game
var p1Score = 0;
var p2Score = 0;

// game logic
var gameOver = false;
var winningScore = 5;

// set the winning score

// manipulate the p1 button
p1Button.addEventListener("click", function() {
    if(!gameOver) {
        p1Score++;
        if(p1Score === winningScore) {
            p1Display.classList.add("winner");
            gameOver = true;
        };
        p1Display.textContent = p1Score;
    };
});

// manipulate the p1 button
p2Button.addEventListener("click", function() {
    if(!gameOver) {
        p2Score++;
        if(p2Score === winningScore) {            
            p2Display.classList.add("winner");
            gameOver = true;
        };
        p2Display.textContent = p2Score;
    };
});

// reset actions
resetButton.addEventListener("click", function() {
    reset();
});

 function reset() {
    // set the scores
    p1Score = 0;
    p2Score = 0;

    // reset content 
    p1Display.textContent = 0;
    p2Display.textContent = 0;
    p1Display.classList.remove("winner");
    p2Display.classList.remove("winner");
    gameOver = false;
}

// event for whenver the value changes
numInput.addEventListener("change", function() {
    winningScoreDisplay.textContent = this.value;
    winningScore = Number(this.value);
    reset();
});