// Node Exercise 1

// Print stuff


// define variable to hold the string to print
var string = "Print number: ";
// define variable to hold the number of times to print the string
var number = 10;

// define the function with above input
function echo(string, number) {
// for loop to print the string with conditional to stop at number var
    for(var i = 0; i < 10; i++) {
        // don't print zero
        if(i != 0) {
            // print the string
            console.log(string, i);
        }

    }
}

// call function with arguments like python.
echo(string, number)