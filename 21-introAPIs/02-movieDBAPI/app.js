// movieDBAI.js

// init packages
var express = require('express');
request = require('request');

// init express and save to var
app = express();

// set ejs as the rendering engine
app.set('view engine', 'ejs');

app.get("/", function (req, res) {
    res.render("search");
});

app.get("/results", function (req, res) {
    var search = req.query.search
    var url = "http://omdbapi.com/?s="
    const apiKey = "thewdb"
    request(url + search + '&apikey=' + apiKey + '&json', (error, response, body) => {
        if (!error && response.statusCode == 200) {
            const results = JSON.parse(body);
            res.render("results", { results: results });
        };
    });
});



// start the server
app.listen("8000");