# udemy-web-dev-course

Code written for the Udemy Web Dev Course.

## Selectors

* Element
    ```css
    li {
  
  }  
    ```
* Class
    ```css
    .name {
  
  }
    ```
* ID
    ```css
    #id {
  
  }
    ```
    
* \* - Select everything
    ````css
    * {
  
  }
    ````
* Descendents
    ```css
   /*Get every anchor descendent of li*/ 
   li a {
  }
    ```
* Adjacent 
    ````css
    
    ````
* Attribute Selector
    ```css
    /*Style all text input boxes.*/
    input[type="text"] {
    
    }
    ```
* nth of type
    ```css
    /* color every other entry*/
    li:nth-of-type(2) {
    background: blue;
    }
    ```