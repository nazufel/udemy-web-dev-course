# RESTful Routing

## Seven RESTful routes
+-------------------------------------------------------------------------------------------------------------------------------------+
| name   | url                   | verb   | desc.                                                   |  Mongoose Method                |
+========+=======================+========+=========================================================+=================================+
| INDEX  | /campgrounds          | GET    | Display a list of all campgrounds                       |  Campground.find()              |
| NEW    | /campgrounds/new      | GET    | Displays form to make a new Campground                  |  N/A                            |
| CREATE | /campgrounds          | POST   | Add a new Campground to DB                              |  Campground.create()            |
| SHOW   | /campgrounds/:id      | GET    | Shows info about one Campground                         |  Campground.findById()          |
| EDIT   | /campgrounds/:id/edit | GET    | Show edit form for one Campground                       |  Campground.findById()          |
| UPDATE | /campgrounds/:id      | PUT    | Update a particular Campground, then redirect somewhere |  Campground.findByIdAndModify() |
| DELETE | /campgrounds/:id      | DELETE | Delete a particular Campground, then redirect somwhere  |  Dog.findByIdAndDelete()        |
+--------+-----------------------+--------+---------------------------------------------------------+---------------------------------+

## Nested Routes
+--------------------------------------------------------------------------------------------------------------+-----------------
| name    | url                           |  verb  | desc.                                                     | Mongoose Methods
+=========+===============================+========+===========================================================+==================     
| NEW     | /campgrounds/:id/comments/new |  GET   | Display the form to create a new comment for a campground |
| CREATE  | /campgrounds/:id/comments     |  POST  | Create a comment for a particular campground              |
+---------+-------------------------------+--------+-----------------------------------------------------------+